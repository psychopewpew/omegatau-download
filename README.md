# Requirements

`Python 3` and `pip` for Python 3.

# Usage

First install the dependencies with
```
pip install -r requirements.txt
pip3 install -r requirements.txt
```
depending on your OS and Python version supported by `pip`.
On most Linux distributions `python`/`pip` refers to Python 2 and `python3`/`pip3` to Python 3.

Then run the script with
```
python main.py
python3 main.py
```
depending on your OS.

The script will download all episodes into the same folder.
It won't download existing episodes.
It **does not** support continuation of download, so if you stop in the middle of a download delete that (incomplete) file. The numbers of all downloaded episodes will all be converted to 4 digit numbers.